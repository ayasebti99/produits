FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} produits-ms.jar
ENTRYPOINT ["java","-jar","/produits-ms.jar"]